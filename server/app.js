// app.js
let express = require("express");
let path = require("path");
let cors = require("cors");
let cookieParser = require("cookie-parser");
let router = require("./api/router/route");
const app = express();
let corsOptions = {
  origin: "http://localhost:3000",
};
app.use(cors(corsOptions));
app.use(express.json());
app.use("/uploadImages", express.static("uploadImages"));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "/public")));
app.use("/api", router);

app.listen(4000, () => {
  console.log("Listen to port 4000");
});

module.exports = app;

const { Pool } = require("pg");
var clt = null;
let controller = {};

const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "root",
  database: "EFireMarket",
});

pool.connect((err, client, release) => {
  console.log("In pool connect");
  if (err) {
    return console.error("Error acquiring client", err.stack);
  } else {
    clt = client;
  }
});

controller.getCategories = async (req, res) => {
  let sql = "SELECT * FROM category";
  try {
    let result = await clt.query(sql);
    res.status(200).json({ category: result.rows });
  } catch (error) {
    console.error(error);
  }
};

module.exports = controller;

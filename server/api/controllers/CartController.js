const { Pool } = require("pg");
var clt = null;
const controller = {};

const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "root",
  database: "EFireMarket",
});

pool.connect((err, client, release) => {
  console.log("In pool connect");
  if (err) {
    return console.error("Error acquiring client", err.stack);
  } else {
    clt = client;
  }
});

controller.addToCart = async (req, res) => {
  let sql = "SELECT * FROM cart c WHERE c.idproduct = $1 AND c.iduser = $2";
  try {
    var result = await clt.query(sql, [req.params.id_product, req.user.id]);
    if (result.rowCount > 0) {
      let new_amount = result.rows[0].amountbought + 1;
      sql =
        "UPDATE cart SET amountbought = $1 WHERE idproduct = $2 AND iduser = $3";
      await clt.query(sql, [new_amount, req.params.id_product, req.user.id]);
      res.sendStatus(200);
    } else {
      sql =
        "INSERT INTO cart (iduser, idproduct, amountbought) VALUES ($1, $2, $3)";
      await clt.query(sql, [req.user.id, req.params.id_product, 1]);
      res.sendStatus(201);
    }
  } catch (error) {
    console.error(error);
  }
};

controller.getCart = async (req, res) => {
  let sql =
    "SELECT c.amountBought, p.idproduct, p.nameproduct, p.priceproduct, p.photoproduct FROM cart c JOIN product p ON p.idproduct = c.idproduct WHERE iduser = $1";
  try {
    let result = await clt.query(sql, [req.user.id]);
    res.status(200).json({ products: result.rows });
  } catch (error) {
    console.error(error);
  }
};

module.exports = controller;

let bcrypt = require("bcrypt");
let jwt = require("jsonwebtoken");

const { Pool } = require("pg");
var clt = null;
let controller = {};

const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "root",
  database: "EFireMarket",
});

pool.connect((err, client, release) => {
  console.log("In pool connect");
  if (err) {
    return console.error("Error acquiring client", err.stack);
  } else {
    clt = client;
  }
});

/**
 * @param id_user int
 */
controller.getInfo = async (req, res) => {
  let sql = "SELECT * FROM useraccount u WHERE u.iduser = $1";
  try {
    let result = await clt.query(sql, [req.user.id]);
    if (result.rowCount > 0) {
      res.status(200).json({ info: result.rows[0] });
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.error(error);
  }
};

controller.register = async (req, res) => {
  let sql = "SELECT * FROM useraccount u WHERE u.emailuser = $1";
  try {
    let result = await clt.query(sql, [req.body.email]);
    if (result.rows.length > 0) {
      res.sendStatus(409);
    } else {
      sql =
        "INSERT INTO useraccount (lastnameuser, firstnameuser, emailuser, passworduser, manager) VALUES($1, $2, $3, $4, $5)";
      await clt.query(sql, [
        req.body.lastname,
        req.body.firstname,
        req.body.email,
        bcrypt.hashSync(req.body.password, 10),
        0,
      ]);
      res.sendStatus(201);
    }
  } catch (error) {
    console.error(error);
  }
};

/**
 * @param email varchar,
 * @param password varchar
 */
controller.login = async (req, res) => {
  let sql = "SELECT * FROM useraccount WHERE emailuser = $1";
  try {
    let result = await clt.query(sql, [req.body.email]);
    if (result.rowCount > 0) {
      if (bcrypt.compareSync(req.body.password, result.rows[0].passworduser)) {
        var token = jwt.sign(
          {
            id: result.rows[0].iduser,
            email: result.rows[0].emailuser,
            username: result.rows[0].passworduser,
          },
          "SECRET"
        );
        res.status(200).json({ token });
      } else {
        res.sendStatus(401);
      }
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.error(error);
  }
};

module.exports = controller;

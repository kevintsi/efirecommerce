const { Pool } = require("pg");
var clt = null;
let controller = {};

const pool = new Pool({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "root",
  database: "EFireMarket",
});

pool.connect((err, client, release) => {
  console.log("In pool connect");
  if (err) {
    return console.error("Error acquiring client", err.stack);
  } else {
    clt = client;
  }
});

controller.getProducts = async (req, res) => {
  let sql = "SELECT * FROM product";
  try {
    let result = await clt.query(sql);
    res.status(200).json({ products: result.rows });
  } catch (error) {
    console.error(error);
  }
};

controller.getProduct = async (req, res) => {
  let sql = "SELECT * FROM product WHERE idproduct = $1 ";
  try {
    let result = await clt.query(sql, [req.params.id_product]);
    res.status(200).json({ products: result.rows[0] });
  } catch (error) {
    console.error(error);
  }
};

controller.updateProduct = async (req, res) => {
  let sql =
    "UPDATE product SET nameproduct = $1, priceproduct = $2, idcategory = $3, descriptionproduct = $4 WHERE idproduct = $5";
  try {
    await clt.query(sql, [
      req.body.name_product,
      req.body.price_product,
      req.body.id_category,
      req.body.description_product,
      req.params.id_product,
    ]);
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
  }
};

controller.deleteProduct = async (req, res) => {
  let sql = "DELETE FROM product WHERE idproduct = $1";
  try {
    await clt.query(sql, [req.params.id_product]);
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
  }
};

controller.createProduct = async (req, res) => {
  console.log(req.file);
  let sql =
    "INSERT INTO product (nameproduct, priceproduct, idcategory, descriptionproduct, photoproduct) VALUES ($1, $2, $3, $4, $5)";
  try {
    await clt.query(sql, [
      req.body.name_product,
      req.body.price_product,
      req.body.id_category,
      req.body.description_product,
      "http://localhost:4000/uploadImages/" + req.file.originalname,
    ]);
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
  }
};

module.exports = controller;

let express = require("express");
let multer = require("multer");

let middleware = require("../middlewares/middlewares");
let UserController = require("../controllers/UserController");
let ProductController = require("../controllers/ProductController");
let CategoryController = require("../controllers/CategoryController");
let CartController = require("../controllers/CartController");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploadImages");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter,
});

let router = express.Router();

/** CRUD + LOGIN user */
router.post("/user/register", UserController.register);
router.post("/user/me", middleware.checkAuth, UserController.getInfo);
router.post("/user/login", UserController.login);

router.get("/products", ProductController.getProducts);
router.get("/product/:id_product", ProductController.getProduct);
router.put("/product/:id_product", ProductController.updateProduct);
router.delete("/product/:id_product", ProductController.deleteProduct);
router.post(
  "/product",
  upload.single("photo_product"),
  ProductController.createProduct
);

router.get("/categories", CategoryController.getCategories);

router.all("/cart/:id_product", middleware.checkAuth, CartController.addToCart);
router.get("/cart", middleware.checkAuth, CartController.getCart);

module.exports = router;

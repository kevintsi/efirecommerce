import React from "react";
import UserService from "./services/UserService";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      firstname: "",
      lastname: "",
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.register = this.register.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.register(this.state);
  }

  async register(user) {
    try {
      let result = await UserService.register(user);
      if (result.status === 201) {
        alert("Inscription  réussie");
        this.props.history.push("/login");
      }
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <div>
        <form method="POST">
          <div>S'enregistrer</div>
          <div>
            Nom :{" "}
            <input
              type="text"
              placeholder="Nom..."
              name="lastname"
              value={this.state.lastname}
              onChange={this.onChange}
            />
          </div>
          <div>
            Prénom :{" "}
            <input
              type="text"
              placeholder="Prénom..."
              name="firstname"
              value={this.state.firstname}
              onChange={this.onChange}
            />
          </div>
          <div>
            Adresse email :{" "}
            <input
              type="email"
              placeholder="Adresse email..."
              name="email"
              value={this.state.email}
              onChange={this.onChange}
            />
          </div>
          <div>
            Mot de passe :{" "}
            <input
              type="password"
              placeholder="Mot de passe..."
              name="password"
              value={this.state.password}
              onChange={this.onChange}
            />
          </div>
          <div>
            <button type="submit" onClick={this.onSubmit}>
              S'enregistrer
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Register;

import React, { Fragment } from "react";
import ProductService from "./services/ProductService";

class DetailProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
    };
  }

  componentDidMount() {
    this.loadDetailProduct(this.props.match.params.id_product);
  }

  async loadDetailProduct(id) {
    try {
      var result = await ProductService.getProductById(id);
      if (result.status === 200) {
        var data = await result.json();
        console.log(data);
        this.setState({
          product: data.products,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    if (Object.keys(this.state.product).length === 0) {
      return <h1>Aucun produit n'est assigné à cet identifiant</h1>;
    } else {
      return (
        <Fragment>
          <div>Detail du produit</div>
          <h4>Nom : {this.state.product.nameproduct}</h4>
          <img
            src={this.state.product.photoproduct}
            alt={this.state.product.nameproduct}
          />
          Prix : {this.state.product.priceproduct}
          <br />
          Description : {this.state.product.descriptionproduct}
        </Fragment>
      );
    }
  }
}

export default DetailProduct;

import React from "react";

class CartProduct extends React.Component {
  render() {
    return (
      <tbody>
        <tr>
          <td>{this.props.product.idproduct}</td>
          <td>{this.props.product.nameproduct}</td>
          <td>
            <img
              src={this.props.product.photoproduct}
              alt={this.props.product.nameproduct}
            />
          </td>
          <td>{this.props.product.priceproduct}</td>
          <td>
            <input
              type="number"
              value={this.props.product.amountbought}
              onChange={() => this.props.onChange(this.props.product.idproduct)}
            />
          </td>
        </tr>
      </tbody>
    );
  }
}

export default CartProduct;

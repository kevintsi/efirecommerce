import React from "react";
import { Route } from "react-router-dom";

const PrivateRouteManager = ({ component: Component, isManager, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isManager ? (
        <Component {...props} />
      ) : (
        alert("Tu dois être manager pour avoir accès à cette page")
      )
    }
  />
);

export default PrivateRouteManager;

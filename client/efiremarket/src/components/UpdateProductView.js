import React from "react";
import ProductService from "./services/ProductService";
import CategoryService from "./services/CategoryService";

class UpdateProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryList: [],
      description_product: "",
      category: "",
      name_product: "",
      photo_product: "",
      price_product: "",
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.loadDetailProduct(this.props.match.params.id_product);
    this.loadCategory();
  }

  async loadCategory() {
    try {
      var result = await CategoryService.getAllCategory();
      if (result.status === 200) {
        var data = await result.json();
        console.log(data);
        this.setState({
          categoryList: data.category,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  onChange(e) {
    console.log(e.target.name);
    console.log(e.target.value);
    console.log(this.state);
    const target = e.target;
    // const value = target.name === 'isGoing' ? target.checked : target.value;
    // const name = target.name;
    this.setState({
      [target.name]: target.value,
    });
    console.log(this.state);
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this.state);

    let updatedProduct = {
      description_product: this.state.description_product,
      id_category: this.state.category,
      name_product: this.state.name_product,
      photo_product: this.state.photo_product,
      price_product: this.state.price_product,
    };

    this.updateProductById(this.props.match.params.id_product, updatedProduct);
  }

  async updateProductById(id_product, updatedProduct) {
    try {
      let result = await ProductService.updateProduct(
        id_product,
        updatedProduct
      );
      //console.log(result)
      if (result.status === 200) {
        alert("Le produit a été mise à jour avec succès");
      }
    } catch (error) {
      console.error(error);
    }
  }

  async loadDetailProduct(id) {
    try {
      var result = await ProductService.getProductById(id);
      if (result.status === 200) {
        var data = await result.json();
        console.log(data);
        this.setState({
          description_product: data.products.descriptionproduct,
          category: String(data.products.idcategory),
          name_product: data.products.nameproduct,
          photo_product: data.products.photoproduct,
          price_product: String(data.products.priceproduct),
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    console.log(this.state.price_product);
    const listCategory = this.state.categoryList.map((category) => {
      if (String(category.idcategory) === this.state.category) {
        return (
          <option
            selected
            key={category.idcategory}
            value={category.idcategory}
          >
            {category.namecategory}
          </option>
        );
      } else {
        return (
          <option key={category.idcategory} value={category.idcategory}>
            {category.namecategory}
          </option>
        );
      }
    });
    return (
      <div>
        Update
        <form onSubmit={this.onSubmit}>
          Nom produit :{" "}
          <input
            name="name_product"
            type="text"
            value={this.state.name_product}
            onChange={this.onChange}
          />
          <br />
          Categorie :
          <select name="category" onChange={this.onChange}>
            {listCategory}
          </select>
          Description :{" "}
          <textarea
            name="description_product"
            value={this.state.description_product}
            onChange={this.onChange}
          />
          Prix :{" "}
          <input
            name="price_product"
            type="number"
            value={this.state.price_product}
            onChange={this.onChange}
          />
          <br />
          <button type="submit" className="btn btn-primary">
            Modifier
          </button>
        </form>
      </div>
    );
  }
}

export default UpdateProduct;

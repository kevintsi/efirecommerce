import React from "react";
import { Route } from "react-router-dom";

const PrivateRouteLoggedIn = ({
  component: Component,
  isLoggedIn,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        alert("Tu dois être connecté pour avoir accès à cette option")
      )
    }
  />
);

export default PrivateRouteLoggedIn;

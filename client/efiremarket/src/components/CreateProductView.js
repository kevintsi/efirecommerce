import React, { Fragment } from "react";
import CategoryService from "./services/CategoryService";
import ProductService from "./services/ProductService";

class CreateProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list_category: [],
      name_product: "",
      category: "",
      description_product: "",
      photo_product: null,
      price_product: "",
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.loadCategory();
  }

  async loadCategory() {
    try {
      var result = await CategoryService.getAllCategory();
      if (result.status === 200) {
        var data = await result.json();
        console.log(data);
        this.setState({
          list_category: data.category,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  onSubmit(e) {
    e.preventDefault();
    console.log(this.state);
    let data = new FormData();
    data.append("name_product", this.state.name_product);
    data.append("id_category", parseInt(this.state.category));
    data.append("description_product", this.state.description_product);
    data.append("price_product", parseFloat(this.state.price_product));
    data.append("photo_product", this.state.photo_product);

    this.createProduct(data);
  }

  async createProduct(data) {
    try {
      let result = await ProductService.createProduct(data);
      if (result.status === 201) {
        alert("Produit ajouté avec succès");
      }
    } catch (error) {
      console.error(error);
    }
  }

  onChange(e) {
    let value = e.target.files ? e.target.files[0] : e.target.value;
    this.setState({
      [e.target.name]: value,
    });
  }
  render() {
    let list_category = this.state.list_category.map((category) => {
      return (
        <option key={category.idcategory} value={category.idcategory}>
          {category.namecategory}
        </option>
      );
    });
    return (
      <Fragment>
        <h1>Ajouter un nouveau produit</h1>
        <form onSubmit={this.onSubmit} encType="multipart/form-data">
          Nom du produit :{" "}
          <input
            name="name_product"
            type="text"
            value={this.state.name_product}
            onChange={this.onChange}
          />
          <br />
          Catégorie :
          <select name="category" onChange={this.onChange}>
            {list_category}
          </select>
          <br />
          Description du produit :{" "}
          <input
            name="description_product"
            type="text"
            value={this.state.description_product}
            onChange={this.onChange}
          />
          <br />
          Photo du produit :{" "}
          <input name="photo_product" type="file" onChange={this.onChange} />
          <br />
          Prix du produit :{" "}
          <input
            name="price_product"
            type="number"
            value={this.state.price_product}
            onChange={this.onChange}
          />
          <br />
          <button className="button btn-primary">Créer</button>
        </form>
      </Fragment>
    );
  }
}

export default CreateProduct;

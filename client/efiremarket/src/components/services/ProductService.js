const ProductService = {}

ProductService.getAllProducts = async () => {
    try {
        var response = await fetch("http://localhost:4000/api/products", {
            method: "GET"
        })
        return response
    } catch (error) {
        console.error(error)
    }
}


ProductService.getProductById = async (id) => {
    try {
        var response = await fetch("http://localhost:4000/api/product/" + id, {
            method: "GET"
        })
        console.log(response)
        return response
    } catch (error) {
        console.error(error)
    }
}

ProductService.updateProduct = async (id, updatedProduct) => {
    try {
        let response = await fetch("http://localhost:4000/api/product/" + id, {
            method: "PUT",
            body: JSON.stringify(updatedProduct),
            headers: {
                "Content-Type": "application/json"
            }
        })
        console.log(response)
        return response
    } catch (error) {
        console.error(error)
    }
}

ProductService.deleteProduct = async (id) => {
    try {
        let response = await fetch("http://localhost:4000/api/product/" + id, {
            method: "DELETE"
        })
        console.log(response)
        return response
    } catch (error) {
        console.error(error)
    }
}

ProductService.createProduct = async (product) => {
    try {
        console.log(product)
        let response = await fetch("http://localhost:4000/api/product/", {
            method: "POST",
            body: product
        })
        console.log(response)
        return response
    } catch (error) {
        console.error(error)
    }
}
export default ProductService
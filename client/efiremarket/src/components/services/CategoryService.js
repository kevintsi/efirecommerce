const CategoryService = {}

CategoryService.getAllCategory = async () => {
    try {
        var response = await fetch("http://localhost:4000/api/categories", {
            method: "GET"
        })
        return response
    } catch (error) {
        console.error(error)
    }
}

export default CategoryService
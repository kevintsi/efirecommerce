const CartService = {}

CartService.addToCart = async (product_id) => {
    try {
        var response = await fetch("http://localhost:4000/api/cart/" + product_id, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        })
        return response
    } catch (error) {
        console.error(error)
    }
}


CartService.getAllProductsInCart = async () => {
    try {
        var response = await fetch("http://localhost:4000/api/cart", {
            method: "GET",
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        })
        return response
    } catch (error) {
        console.error(error)
    }
}

export default CartService
const UserService = {}

UserService.login = async (user) => {
    try {
        var response = await fetch("http://localhost:4000/api/user/login", {
            method: "POST",
            body: JSON.stringify(user),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return response
    } catch (error) {
        console.error(error)
    }
}
UserService.getUserInfo = async () => {
    try {
        var response = await fetch("http://localhost:4000/api/user/me", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("token")
            }
        })
        return response
    } catch (error) {
        console.error(error)
    }
}

UserService.register = async (user) => {
    try {
        var response = await fetch("http://localhost:4000/api/user/register", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
        return response
    } catch (error) {
        console.error(error)
    }
}
export default UserService
import React from "react";
import { Nav, NavItem, NavLink, NavbarText } from "reactstrap";

class Navbar extends React.Component {
  render() {
    console.log("Navbar refreshed");
    console.log(this.props.state);
    if (this.props.state.authenticated) {
      var userInfo = JSON.parse(localStorage.getItem("userInfo"));
      console.log(userInfo);
      var firstname = userInfo.firstname;
      var lastname = userInfo.lastname;
      if (this.props.state.manager) {
        return (
          <nav>
            <Nav>
              <NavItem>
                <NavbarText style={{ textDecoration: "underline" }}>
                  Bonjour Manager {lastname}
                </NavbarText>
              </NavItem>
              <NavItem>
                <NavLink href="/manage">Gérer les produits</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" onClick={this.props.logout}>
                  Se déconnecter
                </NavLink>
              </NavItem>
            </Nav>
          </nav>
        );
      } else {
        return (
          <nav>
            <Nav>
              <NavItem>
                <NavbarText style={{ textDecoration: "underline" }}>
                  Bonjour Mr {lastname + " " + firstname}
                </NavbarText>
              </NavItem>
              <NavItem>
                <NavLink href="/product">Produits</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/cart">Panier</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" onClick={this.props.logout}>
                  Se déconnecter
                </NavLink>
              </NavItem>
            </Nav>
          </nav>
        );
      }
    } else {
      return (
        <nav>
          <Nav>
            <NavItem>
              <NavLink href="/product">Produits</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/login">Se connecter</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/register">S'enregistrer</NavLink>
            </NavItem>
          </Nav>
        </nav>
      );
    }
  }
}

export default Navbar;

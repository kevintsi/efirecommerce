import React, { Fragment } from "react";
import { NavLink } from "reactstrap";
import CartService from "./services/CartService";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.addToCart = this.addToCart.bind(this);
  }

  async addToCart() {
    try {
      let product_id = this.props.product.idproduct;
      let result = await CartService.addToCart(product_id);
      if (result.status === 201) {
        alert("Produit ajouté au panier avec succès");
      } else if (result.status === 200) {
        alert("Mise à jour du panier exécutée avec succès");
      } else {
        alert(
          "Echec d'ajout/mise à jour du/au panier? Vous devez vous connecter pour avoir accès à cette fonction"
        );
      }
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <Fragment>
        <h4>Nom : {this.props.product.nameproduct}</h4>
        <img
          src={this.props.product.photoproduct}
          alt={this.props.product.nameproduct}
        />
        Prix : {this.props.product.priceproduct}
        <button onClick={this.addToCart}>Ajouter au panier</button>
        <NavLink href={"/product/" + this.props.product.idproduct}>
          En savoir plus...
        </NavLink>
        <br />
        <br />
      </Fragment>
    );
  }
}

export default Product;

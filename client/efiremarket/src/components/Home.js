import React from "react";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.user = JSON.parse(localStorage.getItem("userInfo"));
  }
  render() {
    return (
      <div>
        Home {this.user ? this.user.firstname + " " + this.user.lastname : ""}
      </div>
    );
  }
}

export default Home;

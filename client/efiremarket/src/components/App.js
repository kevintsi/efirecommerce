import React from "react";
import Routes from "./Routes";
import Navbar from "./Navbar";
import { BrowserRouter as Router } from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated:
        !!localStorage.getItem("userInfo") && !!localStorage.getItem("token"),
      manager:
        !!localStorage.getItem("userInfo") &&
        JSON.parse(localStorage.getItem("userInfo")).manager,
    };
    console.log(this.state.authenticated);
    this.setAuthenticated = this.setAuthenticated.bind(this);
    this.logout = this.logout.bind(this);
  }
  setAuthenticated(isAuthenticated, isManager) {
    this.setState({
      authenticated: isAuthenticated,
      manager: isManager,
    });
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("userInfo");
    window.location.href = "http://localhost:3000";
  }

  render() {
    console.log("App refreshed");
    return (
      <div>
        <Router>
          <Navbar logout={this.logout} state={this.state} />
          <Routes
            stateUser={this.state}
            setAuthenticated={this.setAuthenticated}
          />
        </Router>
      </div>
    );
  }
}

export default App;

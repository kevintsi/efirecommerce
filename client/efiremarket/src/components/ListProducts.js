import React from "react";
import ProductService from "./services/ProductService";
import Product from "./Product";

class ListProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }
  componentDidMount() {
    this.loadProducts();
  }

  async loadProducts() {
    try {
      var result = await ProductService.getAllProducts();
      var data = await result.json();
      var products = data.products;
      this.setState({
        products,
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    var listProducts = this.state.products.map((product) => (
      <Product key={product.idproduct} product={product} />
    ));
    return (
      <div>
        <div>Liste des produits</div>
        {listProducts}
      </div>
    );
  }
}

export default ListProduct;

import React, { Fragment } from "react";
import ProductService from "./services/ProductService";

class ManageProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    this.loadProducts();
  }

  async loadProducts() {
    try {
      var result = await ProductService.getAllProducts();
      var data = await result.json();
      var products = data.products;
      this.setState({
        products,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async onDelete(id) {
    if (window.confirm("Voulez-vous vraiment supprimer ce produit ?")) {
      await ProductService.deleteProduct(id);
      this.setState((prevState) => {
        var newListProduct = prevState.products.filter(
          (product) => product.idproduct !== id
        );
        console.log(newListProduct);
        return { products: newListProduct };
      });
    }
  }

  render() {
    var listProducts = this.state.products.map((product) => {
      return (
        <tr key={product.idproduct}>
          <td>{product.idproduct}</td>
          <td>{product.nameproduct}</td>
          <td>{product.priceproduct}</td>
          <td>{product.namecategory}</td>
          <td>{product.descriptionproduct}</td>
          <td>
            <img src={product.photoproduct} alt={product.nameproduct} />
          </td>
          <td>
            <button
              type="submit"
              className="btn btn-primary"
              onClick={() => {
                window.location.href = "/manage/update/" + product.idproduct;
              }}
            >
              Modifier
            </button>
          </td>
          <td>
            <button
              type="submit"
              className="btn btn-danger"
              onClick={() => {
                this.onDelete(product.idproduct);
              }}
            >
              Supprimer
            </button>
          </td>
        </tr>
      );
    });
    if (listProducts.length === 0) {
      return (
        <Fragment>
          <div>Manage Product</div>
          Il n'y a aucun produit d'enregistré
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <div>Manage Product</div>
          <button
            className="button btn-primary"
            onClick={() => {
              window.location.href = "/manage/create";
            }}
          >
            Ajouter un produit <i class="fas fa-plus"></i>
          </button>
          <table>
            <thead>
              <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Prix</th>
                <th>Catégorie</th>
                <th>Description</th>
                <th>Image</th>
              </tr>
            </thead>
            <tbody>{listProducts}</tbody>
          </table>
        </Fragment>
      );
    }
  }
}

export default ManageProduct;

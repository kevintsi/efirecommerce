import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./Login";
import Home from "./Home";
import ListProducts from "./ListProducts";
import DetailProduct from "./DetailProductView";
import CreateProduct from "./CreateProductView";
import ManageProduct from "./ManageProductView";
import UpdateProduct from "./UpdateProductView";
import Cart from "./CartView";
import Register from "./Register";
import PrivateRouteManager from "./PrivateRouteManager";
import PrivateRouteLoggedIn from "./PrivateRouteLoggedIn";

class Routes extends React.Component {
  render() {
    console.log("Routes refreshed");
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/product" component={ListProducts} />
        <Route path="/product/:id_product" component={DetailProduct} />
        <Route path="/register" component={Register} />
        <PrivateRouteManager
          exact
          path="/manage"
          isManager={this.props.stateUser.manager}
          component={ManageProduct}
        />
        <PrivateRouteManager
          path="/manage/create"
          isManager={this.props.stateUser.manager}
          component={CreateProduct}
        />
        <PrivateRouteManager
          path="/manage/update/:id_product"
          isManager={this.props.stateUser.manager}
          component={UpdateProduct}
        />
        <PrivateRouteLoggedIn
          path="/cart"
          isLoggedIn={this.props.stateUser.authenticated}
          component={Cart}
        />
        <Route
          path="/login"
          render={(props) => (
            <Login {...props} setAuthenticated={this.props.setAuthenticated} />
          )}
        />
      </Switch>
    );
  }
}

export default Routes;

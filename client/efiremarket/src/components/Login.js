import React from "react";
import UserService from "./services/UserService";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      nbTry: 0,
      message: "",
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    console.log(this.props);
  }
  componentDidMount() {
    if (!!localStorage.getItem("userInfo")) {
      this.props.history.push("/");
    }
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  async onSubmit(e) {
    if (this.state.message) return;
    e.preventDefault();
    console.log(this.state);
    var user = {
      email: this.state.email,
      password: this.state.password,
    };
    var response = await UserService.login(user);
    console.log(response);

    if (response.status === 200) {
      const data = await response.json();
      console.log(data.token);
      localStorage.setItem("token", data.token);
      response = await UserService.getUserInfo();
      if (response.status === 200) {
        const data = await response.json();
        console.log(data);
        var userInfo = {
          lastname: data.info.lastnameuser,
          firstname: data.info.firstnameuser,
          manager: data.info.manager,
        };
        localStorage.setItem("userInfo", JSON.stringify(userInfo));
        this.props.setAuthenticated(true, userInfo.manager);
        this.props.history.push("/");
      } else if (response.status === 401) {
        console.log("Token provided incorrect");
      }
    } else if (response.status === 401) {
      if (this.state.nbTry >= 5) {
        this.setState({
          message: "Tu as fait trop d'essais, réessaye dans 30 s",
        });
        setTimeout(() => {
          this.setState({
            message: "",
            nbTry: 0,
          });
        }, 30000);
      }
      this.setState((previousState) => {
        return {
          nbTry: (previousState.nbTry += 1),
        };
      });
      console.log("Your email or/and password is/are wrong");
    }
  }
  render() {
    return (
      <div>
        <form method="POST">
          {this.state.message ? <div>{this.state.message}</div> : null}
          <div>Identification</div>
          <div>
            Adresse email :{" "}
            <input
              type="email"
              disabled={this.state.message}
              placeholder="Adresse email..."
              name="email"
              value={this.state.email}
              onChange={this.onChange}
            />
          </div>
          <div>
            Mot de passe :{" "}
            <input
              type="password"
              disabled={this.state.message}
              placeholder="Mot de passe..."
              name="password"
              value={this.state.password}
              onChange={this.onChange}
            />
          </div>
          <div>
            <button
              type="submit"
              disabled={this.state.message}
              onClick={this.onSubmit}
            >
              Se connecter
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;

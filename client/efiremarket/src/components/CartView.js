import React, { Fragment } from "react";
import CartService from "./services/CartService";
import CartProduct from "./CartProduct";

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productListInCart: [],
    };
    this.onChange = this.onChange.bind(this);
    this.getTotalPrice = this.getTotalPrice.bind(this);
  }

  componentDidMount() {
    this.getProductsInCart();
  }

  getTotalPrice() {
    let totalPrice = 0;
    if (this.state.productListInCart.length > 0) {
      this.state.productListInCart.forEach((product) => {
        totalPrice += product.amountbought * product.priceproduct;
      });
    }
    return totalPrice;
  }

  async getProductsInCart() {
    let response = await CartService.getAllProductsInCart();
    if (response.status === 200) {
      let data = await response.json();
      console.log(data);
      this.setState({
        productListInCart: data.products,
      });
    } else {
      alert(
        "Un problème est survenu lors de la récupération des produits dans le panier"
      );
    }
  }

  onChange(idproduct) {
    this.setState((prevState) => {
      return {
        productListInCart: prevState.productListInCart.map((product) => {
          return product.idproduct === idproduct
            ? { ...product, amountbought: (product.amountbought += 1) }
            : product;
        }),
      };
    });
  }

  render() {
    return (
      <Fragment>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Nom du produit</th>
              <th>Photo du produit</th>
              <th>Prix du produit</th>
              <th>Quantité</th>
            </tr>
          </thead>
          {this.state.productListInCart.length > 0 ? (
            this.state.productListInCart.map((product) => {
              return (
                <CartProduct
                  key={product.idproduct}
                  product={product}
                  onChange={this.onChange}
                />
              );
            })
          ) : (
            <tbody>
              <tr>
                <td>Il n'y a pas de produit dans ce panier</td>
              </tr>
            </tbody>
          )}
          <tfoot>
            <tr>
              <td colSpan="5">Prix total : {this.getTotalPrice()} euros</td>
            </tr>
          </tfoot>
        </table>
      </Fragment>
    );
  }
}

export default Cart;
